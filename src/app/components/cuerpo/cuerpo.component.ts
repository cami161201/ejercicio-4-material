import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css'],
})
export class CuerpoComponent implements OnInit {
  form!: FormGroup;
contenido:string[]=[]
  constructor(private formBuilder: FormBuilder) {
    this.crearCajas();
  }

  ngOnInit(): void {}

  crearCajas() {
    this.form = this.formBuilder.group({  
      cajaTexto: this.formBuilder.array([[]]),
    });
  }

  get contenidoCaja() {
    return this.form.get('cajaTexto') as FormArray;
   } 


  // el boton debe generar cajas de texto
  adicionarCaja():void {
    this.contenidoCaja.push(this.formBuilder.control(''));
  }

  //limpiar la caja de texto Multilinea
  limpiarMultitexto():void {
    this.contenido=['']
  }

  //eliminar la caja de texto
  eliminarCaja(i:number):void {
    this.contenidoCaja.removeAt(i);
  }

  //Guarda la informacion de las cajas de texto dinamicas en la caja de texto multilinea
  guardarCajas():void {
    console.log("Guardando");
    this.contenido=this.form.value.cajaTexto
  }

// no da
  limpiarCajaDinamica():void {
    this.form.value.cajaTexto.reset
  }

  //limpia todas las cajas de texto
  limpiarTodo(): void {
    this.contenido = ['']
    this.form.reset
  }
}
